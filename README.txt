********************************************************************
           PASSED DURATION    M O D U L E
********************************************************************
Original Author: Brajendra Singh
Current Maintainers: Brajendra Singh

********************************************************************
DESCRIPTION:

	Provide a CCK field type that lets you choose two drop down fields like year & month.
	Display output of this field shows no of years and months.
	This module will be mostly used wherever past duration needs to display in year and month format.
	For Ex: In any job portal site you need to add a field for experience, this module will help it out.
	Integration with views module.
   
********************************************************************
INSTALLATION:

1. Place the entire passedduration directory into your Drupal modules
   directory or the sites modules directory (eg site/default/modules)


2. Enable this module by navigating to:

     Administer > Build > Modules
  

********************************************************************
NEW AUTHOR & MAINTAINER (16 Dec 2011)

	This module is no longer "deprecated".
	As the new author & maintainer, I plan to - 
		Improve code quality.
		Fix any problems I run into.
		Be responsive on the issue queue.
		Slowly work through older issues (with a better chance if you make some noise).
		Allow a co-maintainer to jump on the boat, if someone is interested.
		Some day, produce a D7 version
		There are no particular bugs I want to fix (yet), I just want this module to be not abandoned.
		
********************************************************************
